# Typer Game

Typer Game works by inputting a URL to a text file with a commit hash in the URL, e.g. GitLab or GitHub. If you want to upload your own file you can create a GitLab Snippet or GitHub Gist and then input that URL. 

Then, you type over the top of the file and when you complete it 100% then your time is added to the leaderboard for that URL along with your username.

The initial prototype doesn't measure your typing speed or accuracy, just if you completed it or not, which it measures by hashing your input against the source file and seeing if it matches or not.
